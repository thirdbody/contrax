# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- N/A

### Changed
- N/A

### Removed
- N/A

## [1.1.1] - 2020-06-30
### Added
- Now warning users about using non-Chromium browsers due to present issue with Safari compatibility (temp. fix)

### Changed
- Fixed CHANGELOG unreleased link to go from `v1.1.0` to `master`

### Removed
- N/A

## [1.1.0] - 2020-06-29
### Added
- Created CHANGELOG.md
- Created CONTRIBUTING.md

### Changed
- Including possibility of wrong 2-factor code in authentication rejection message
- Enabling administrator to send a file to every user using the wildcard (*).
- Reminding admin to reload the page to refresh their tabs.

### Removed
- N/A

## [1.0.0] - 2020-06-28
### Added
- Initial project version

### Changed
- N/A

### Removed
- N/A

[Unreleased]: https://gitlab.com/myl0g/contrax/compare/v1.1.1...master
[1.1.1]: https://gitlab.com/myl0g/contrax/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/myl0g/contrax/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/myl0g/contrax/compare/v0.0.1...v1.0.0
