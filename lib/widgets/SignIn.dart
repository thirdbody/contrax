import 'package:contrax/widgets/Register2fa.dart';
import 'package:dbcrypt/dbcrypt.dart';
import 'package:flutter/material.dart';
import 'package:platform_detect/platform_detect.dart';

import '../api.dart';
import 'MainScreen.dart';

class SignInScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Center(
        child: SizedBox(
          width: 400,
          child: Card(
            child: SignInForm(),
          ),
        ),
      ),
    );
  }
}

class SignInForm extends StatefulWidget {
  @override
  _SignInFormState createState() => _SignInFormState();
}

class _SignInFormState extends State<SignInForm> {
  final _usernameTextController = TextEditingController();
  final _passwordTextController = TextEditingController();
  final _totpTextController = TextEditingController();

  void _registerUser() async {
    final api = API(_usernameTextController.text);
    api.salt = new DBCrypt().gensalt();
    final hash = new DBCrypt().hashpw(_passwordTextController.text, api.salt);
    _passwordTextController.text = null;

    final qrCodeRaw = await api.registerTotp(hash);
    if (qrCodeRaw == null) {
      showDialog(
          context: context,
          child: AlertDialog(
            title: const Text("Registration Error"),
            content: const SingleChildScrollView(
              child: const Text(
                  "Please contact your system administrator to resolve this issue."),
            ),
            actions: [
              FlatButton(
                child: const Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          ));
      return;
    }

    showDialog(
      context: context,
      child: Register2fa(
        image: qrCodeRaw,
        api: api,
        hash: hash,
      ),
    );
  }

  void _loginUser() async {
    final api = API(_usernameTextController.text);
    await api.updateSaltFromServer();
    final hash = new DBCrypt().hashpw(_passwordTextController.text, api.salt);
    _passwordTextController.text = null;

    if (await api.authenticate(hash, _totpTextController.text)) {
      final incomingFiles = await api.getIncomingFileNames();
      final outgoingFiles = await api.getOutgoingFileNames();

      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => MainScreen(
            api: api,
            incomingFiles: incomingFiles,
            outgoingFiles: outgoingFiles,
          ),
        ),
      );
    } else {
      showDialog(
          context: context,
          child: AlertDialog(
            title: const Text("Login Error"),
            content: const SingleChildScrollView(
              child: const Text(
                "Either your username, password, or 2-Factor code was incorrect.",
              ),
            ),
            actions: [
              FlatButton(
                child: const Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    if (!browser.isChrome) {
      return AlertDialog(
        title: const Text("Browser Unsupported"),
        content: const SingleChildScrollView(
          child: const Text(
            "We do not currently support browsers that aren't based on Chromium (such as Google Chrome).",
          ),
        ),
        actions: [
          FlatButton(
            child: const Text("OK"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ],
      );
    }

    return Form(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("Sign In", style: Theme.of(context).textTheme.headline4),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: _usernameTextController,
              decoration: const InputDecoration(hintText: "Username"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: _passwordTextController,
              obscureText: true,
              decoration: const InputDecoration(hintText: "Password"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: _totpTextController,
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(hintText: "2-Factor Code"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: const Text(
                "If you're registering as a user, do not provide a 2-Factor code."),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FlatButton(
                color: Colors.green,
                textColor: Colors.white,
                onPressed: _loginUser,
                child: const Text("Login"),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
              ),
              FlatButton(
                color: Colors.green,
                textColor: Colors.white,
                onPressed: _registerUser,
                child: const Text("Register"),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
