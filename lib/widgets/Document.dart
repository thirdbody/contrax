import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' as html;

import '../api.dart';

class DocumentPage extends StatelessWidget {
  final API api;
  final List<String> fileNames;

  DocumentPage({Key key, @required this.api, @required this.fileNames});

  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      child: ListView(
        children: _tiles(context),
      ),
    );
  }

  ListTile _tile(BuildContext context, String documentName) {
    return ListTile(
      title: Text(
        documentName,
        style: const TextStyle(fontSize: 18.0),
      ),
      trailing: const Icon(Icons.arrow_forward),
      onTap: () {
        showDialog(
            context: context, child: DocumentView(documentName, api: api));
      },
    );
  }

  List<ListTile> _tiles(BuildContext context) {
    return this.fileNames.map((fileName) => _tile(context, fileName)).toList();
  }
}

class DocumentView extends StatelessWidget {
  final String documentName;
  final API api;

  DocumentView(this.documentName, {@required this.api});

  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 700,
        child: Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Text(
                  this.documentName,
                  style: Theme.of(context).textTheme.headline4,
                  textAlign: TextAlign.center,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FlatButton.icon(
                    onPressed: () async {
                      final blob = html.Blob(
                          [await api.downloadPDF(documentName)],
                          'application/pdf');
                      final url = html.Url.createObjectUrlFromBlob(blob);
                      // html.window.open(url, "_blank");
                      // html.Url.revokeObjectUrl(url);
                      final anchorElement = new html.AnchorElement(href: url);
                      anchorElement.download = this.documentName;
                      anchorElement.click();
                    },
                    icon: const Icon(Icons.cloud_download),
                    label: const Text("Download PDF"),
                  ),
                  FlatButton.icon(
                    label: const Text("Upload Signed Copy"),
                    icon: const Icon(Icons.file_upload),
                    onPressed: () async {
                      html.InputElement uploadInput =
                          html.FileUploadInputElement();
                      uploadInput.multiple = false;
                      uploadInput.draggable = true;
                      uploadInput.accept = ".pdf";
                      uploadInput.click();

                      uploadInput.onChange.listen((event) {
                        final file = uploadInput.files[0];
                        final reader = html.FileReader();
                        reader.onLoadEnd.listen((e) async {
                          final uploadStatus = await (api.uploadPDF(
                            reader.result,
                            "Signed - " + documentName,
                          ));

                          if (uploadStatus) {
                            showDialog(
                                context: context,
                                child: AlertDialog(
                                  title: const Text("Success"),
                                  content: const SingleChildScrollView(
                                    child: const Text(
                                        "Your file uploaded successfully!"),
                                  ),
                                  actions: [
                                    FlatButton(
                                      child: const Text("OK"),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    )
                                  ],
                                ));
                          } else {}
                        });

                        reader.readAsArrayBuffer(file);
                      });
                    },
                  ),
                  FlatButton.icon(
                    icon: const Icon(
                      Icons.close,
                    ),
                    label: const Text("Close"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
