import 'dart:typed_data';

import 'package:contrax/api.dart';
import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' as html;

class Register2fa extends StatelessWidget {
  final Uint8List image;
  final API api;
  final String hash;
  final _totpController = TextEditingController();

  Register2fa({@required this.image, @required this.api, @required this.hash});

  Widget build(BuildContext context) {
    final qr = MemoryImage(this.image);

    return Center(
      child: Container(
        width: 300,
        child: Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: const Text(
                  "All users MUST enroll in 2-Factor Authentication (2FA) before using this system.",
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: const Text(
                  "Download an app such as FreeOTP or Google Authenticator from your mobile device's app store.",
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: const Text(
                  "Using that app, please scan the below code.",
                ),
              ),
              Image(
                image: qr,
              ),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: const Text(
                  "Once you've done so, enter the generated code below.",
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: this._totpController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(hintText: "Enter code here"),
                ),
              ),
              FlatButton.icon(
                onPressed: () async {
                  if (await api.authenticate(hash, _totpController.text)) {
                    showDialog(
                        context: context,
                        child: AlertDialog(
                          title: const Text("Success"),
                          content: const SingleChildScrollView(
                            child:
                                const Text("You have successfully registered!"),
                          ),
                          actions: [
                            FlatButton(
                              child: const Text("OK"),
                              onPressed: () {
                                html.window.location.reload();
                              },
                            )
                          ],
                        ));
                  } else {
                    showDialog(
                        context: context,
                        child: AlertDialog(
                          title: const Text("Failure"),
                          content: const SingleChildScrollView(
                            child: const Text(
                                "The code you supplied was incorrect. Please try again."),
                          ),
                          actions: [
                            FlatButton(
                              child: const Text("OK"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            )
                          ],
                        ));
                  }
                },
                icon: const Icon(Icons.lock),
                label: const Text("Submit"),
              )
            ],
          ),
        ),
      ),
    );
  }
}
